import datetime
print("\nPython Date time module\n")
x=datetime.datetime.now()
print("Today's date and time is: ",x)
print("Year is: ",x.year)
print("Today is: ",x.strftime("%A"))
print("Current month is: ",x.strftime("%B"))
print("Today's date in local date format: ",x.strftime("%D"))
print("Current time: ",x.strftime("%X"))
print("AM/PM: ",x.strftime("%p"))
y= datetime.datetime(2020, 5, 17)
print("Displaying a particular date, for example: ",y)
print("Diplaying in month date year format: ",y.strftime("%b %d %Y %H:%M:%S"))


import time
print("\nPython time module\n")
x= time.time();
local_time = time.ctime(x)
print("Local time: ", local_time)
curtime = time.localtime()
print("Current Time is: ", curtime)
print("year: ", curtime.tm_year)
print("hour: ", curtime.tm_hour)



import calendar
print("\nPython calendar module\n")
calndr = calendar.month(2016, 6)
print("The calendar for a particular month and year, for example: ")
print(calndr)
print("To display a calander by taking inputs from user")
year=int(input("Enter year to view: "))
month=int(input("Enter month to view: "))
x=calendar.month(year, month)
print(x)
print("To print calander for an year")
year=int(input("Enter the year: "))
s = calendar.prcal(year) 

import math
print("\nPython math module\n")
print("The value of e is: ",math.e)
print("The value of pi is: ",math.pi)
print("To do different operation on two numbers")
a=int(input("Enter the first number: "))
b=int(input("Enter the second number number: "))
print("%d power %d is " %(a,b),end="")
print(math.pow(a,b))
print("The gcd of %d and %d is " %(a,b),end="")
print(math.gcd(a,b))
print("The factorial of %d is " %a,end="")
print(math.factorial(a))
print("The factorial of %d is " %b,end="")
print(math.factorial(b))
print("The Square root of %d is " %a,end="")
print(math.sqrt(a))
print("The Square root of %d is " %b,end="")
print(math.sqrt(b))
print("To calculate area of the circle")
r=int(input("Enter the radius of the circle: "))
print("The area of the circle is: ",(math.pi)*r*r)



import string
print("\nPython string module\n")
print("Albpabets: ",string.ascii_letters)
print("Lower case alphabets: ",string.ascii_lowercase)
print("Upper case alphabets: ",string.ascii_uppercase)
print("Digits from 0 to 9: ",string.digits)
print("hexdecimal letters: ",string.hexdigits)
print("whitespace: ")
print(string.whitespace)
print(("Punctuations: "),string.punctuation)
str=input("Enter a string: ")
print("Capitalized form is: ",string.capwords(str))

