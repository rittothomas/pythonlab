import math
def asphere(r):
	return 4*(math.pi)*r*r
def vsphere(r):
	return (4/3)*(math.pi)*r*r*r